/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.inheritanceshape;

/**
 *
 * @author ACER
 */
public class TestShape {
    public static void main(String[] args) {
        Circle c1 = new Circle(3);
        c1.print();
        Circle c2 = new Circle(4);
        c2.print();
        
        Rectangle rect = new Rectangle(4,3);
        rect.print();
        
        Triangle tri = new Triangle (4,3);
        tri.print();
        
        Square squa = new Square(2);
        squa.print();
        
        Shape [] shapes = {c1,c2,rect,tri,squa};
        for(int i = 0 ; i < shapes.length ; i++) {
            shapes[i].print();
        }
    }
            
}
