/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.inheritanceshape;

/**
 *
 * @author ACER
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    
    public Triangle (double base , double height) {
        if(base <= 0 || height <= 0) {
            System.out.println("ERROR : base of height must more than 0 !!!");
        }
        this.base = base;
        this.height = height;
    }
    
    @Override
        public double calArea() {
        return (base * height) / 2;
    }
    
    @Override
    public void print() {
        System.out.println("Rectangle base : " + this.base + " height : " + this.height + " Area is = " + calArea());
                
    }
    
}
